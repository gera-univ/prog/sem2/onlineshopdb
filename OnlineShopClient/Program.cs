﻿using System;
using System.Data.OleDb;
using System.Windows.Forms;
using OnlineShopDAL.DataOperations;

namespace OnlineShopClient
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Select a database file\n");

            OpenFileDialog fileDialog = new OpenFileDialog
            {
                Filter = "Access database files (*.accdb)|*.accdb|All files (*.*)|*.*"
            };
            fileDialog.ShowDialog();

            var connectionStringBuilder = new OleDbConnectionStringBuilder
            {
                DataSource = fileDialog.FileName,
                Provider = "Microsoft.ACE.OLEDB.12.0"
            };
            CourierDAL dal = new CourierDAL(connectionStringBuilder.ConnectionString);

            string answer;
            do
            {
                Console.WriteLine("Enter courier Id to delete the courier\n\t\tl - list all couriers\n\t\tq - quit.\n");
                answer = Console.ReadLine();
                try
                {
                    var id = short.Parse(answer);
                    Console.WriteLine($"Deleting courier {id}");
                    dal.ProcessDeleteCourier(id);
                }
                catch (System.FormatException)
                {
                }
                if (answer == "l")
                    PrintCouriers(dal);
            } while (answer != "q");
        }

        private static void PrintCouriers(CourierDAL dal)
        {
            try
            {
                var list = dal.GetAllCouriers();
                Console.WriteLine(" ************** All Couriers ************** ");
                Console.WriteLine("ID\t\t\tName\t\t\tContact");
                foreach (var itm in list)
                {
                    Console.WriteLine($"{itm.Id}\t\t\t{itm.Name}\t\t\t{itm.Contact}");
                }
            }
            catch (System.Data.OleDb.OleDbException)
            {
                Console.WriteLine("Database exception. Did you select the correct file?");
            }
        }
    }
}
