﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Resources;
using OnlineShopDAL.Models;

namespace OnlineShopDAL.DataOperations
{
    public class CourierDAL
    {
        private readonly string _connectionString;
        private OleDbConnection _oleDbConnection = null;

        ResourceManager _rm = new ResourceManager($"{nameof(OnlineShopDAL)}.Names",
            typeof(CourierDAL).Assembly);

        public CourierDAL(string connectionString)
            => _connectionString = connectionString;

        private void OpenConnection()
        {
            _oleDbConnection = new OleDbConnection { ConnectionString = _connectionString };
            _oleDbConnection.Open();
        }

        private void CloseConnection()
        {
            if (_oleDbConnection?.State != ConnectionState.Closed)
            {
                _oleDbConnection?.Close();
            }
        }

        public void ProcessDeleteCourier(short id)
        {
            OpenConnection();

            var cmdRemoveCourier = new OleDbCommand($"Delete from {_rm.GetString("CourierTable")} where {_rm.GetString("CourierId")} = {id}", _oleDbConnection);
            var cmdEmptyCourierId = new OleDbCommand($"Update {_rm.GetString("OrderTable")} Set {_rm.GetString("CourierId")} = NULL where {_rm.GetString("CourierId")} = {id}", _oleDbConnection);

            OleDbTransaction tx = null;
            try
            {
                tx = _oleDbConnection.BeginTransaction();

                cmdRemoveCourier.Transaction = tx;
                cmdEmptyCourierId.Transaction = tx;

                cmdEmptyCourierId.ExecuteNonQuery();
                cmdRemoveCourier.ExecuteNonQuery();

                tx.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Error occured! Rolling back.");
                tx?.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }

        public List<Courier> GetAllCouriers()
        {
            OpenConnection();
            List<Courier> result = new List<Courier>();

            string sql = $"Select * From {_rm.GetString("CourierTable")}";
            using (OleDbCommand command = new OleDbCommand(sql, _oleDbConnection))
            {
                command.CommandType = CommandType.Text;
                OleDbDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
                while (dataReader.Read())
                {
                    result.Add(new Courier
                    {
                        Id = (short)dataReader[_rm.GetString("CourierId") ?? "unknown"],
                        Name = (string)dataReader[_rm.GetString("CourierName") ?? "unknown"],
                        Contact = (string)dataReader[_rm.GetString("CourierContact") ?? "unknown"]
                    });
                }

                dataReader.Close();
            }

            return result;
        }
    }
}