﻿namespace OnlineShopDAL.Models
{
    public class Courier
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
    }
}